
#include "SwarmController.h"

GTHLD::SwarmController::SwarmController(EngineSystem* es)
{
	m_pParticle = new Particle[NPARTICLE];
	m_es = es;
	m_threadCount = std::thread::hardware_concurrency();
	m_particlesPerThread = NPARTICLE / m_threadCount;
	m_particlesInLastThread = m_particlesPerThread + (NPARTICLE - m_particlesPerThread * m_threadCount);
	for (int i = 0; i < m_threadCount; i++) {
		m_threadStarts.push_back(&m_pParticle[i * m_particlesPerThread]);
	}

	std::cout << m_threadCount << std::endl;
}
GTHLD::SwarmController::~SwarmController()
{
	delete m_pParticle;
}
void GTHLD::SwarmController::manipulateParticles(double mouseX, double mouseY, double mouseMass, Uint32 col)
{
	if (mouseX < 0)mouseX = 0;
	if (mouseX > EngineSystem::SCREEN_WIDTH)mouseX = EngineSystem::SCREEN_WIDTH - 1;
	if (mouseY < 0)mouseY = 0;
	if (mouseY > EngineSystem::SCREEN_HEIGHT)mouseY = EngineSystem::SCREEN_HEIGHT - 1;

	for (int i = 0; i < NPARTICLE; i++) {
		double dist_t = sqrt(pow((mouseX - m_pParticle[i].getX()), 2.0) + pow((mouseY - m_pParticle[i].getY()), 2.0));
		double ratio_x = mouseX - m_pParticle[i].getX();
		double ratio_y = mouseY - m_pParticle[i].getY();
		double dirX = 1;
		double dirY = 1;
		if (ratio_x < 0) {
			ratio_x = m_pParticle[i].getX() - mouseX;
			dirX = -1;
		}
		if (ratio_y < 0) {
			ratio_y = m_pParticle[i].getY() - mouseY;
			dirY = -1;
		}
		double force_t = (m_gConst * mouseMass * m_pParticle[i].getM()) / (pow(dist_t, 2.0));
		double force_x = force_t * ratio_x * dirX;
		double force_y = force_t * ratio_y * dirY;
		m_pParticle[i].calculateTick(force_x, force_y, m_es, col);
	}



}


void applyParticleForces(double mouseX, double mouseY, double mouseMass, Uint32 col, int thread, GTHLD::Particle* particlePtr, int m_particlesPerThread, int m_threadCount, int m_particlesInLastThread, double m_gConst, GTHLD::EngineSystem* m_es)
{
	if (mouseX < 0)mouseX = 0;
	if (mouseX > GTHLD::EngineSystem::SCREEN_WIDTH)mouseX = GTHLD::EngineSystem::SCREEN_WIDTH - 1;
	if (mouseY < 0)mouseY = 0;
	if (mouseY > GTHLD::EngineSystem::SCREEN_HEIGHT)mouseY = GTHLD::EngineSystem::SCREEN_HEIGHT - 1;
	int iteratorSize = m_particlesPerThread;
	if (thread == m_threadCount - 1)iteratorSize = m_particlesInLastThread;
	
	for (int i = 0; i < iteratorSize; i++) {
		double dist_t = sqrt(pow((mouseX - particlePtr[i].getX()), 2.0) + pow((mouseY - particlePtr[i].getY()), 2.0));
		double ratio_x = mouseX - particlePtr[i].getX();
		double ratio_y = mouseY - particlePtr[i].getY();
		double dirX = 1;
		double dirY = 1;
		if (ratio_x < 0) {
			ratio_x = particlePtr[i].getX() - mouseX;
			dirX = -1;
		}
		if (ratio_y < 0) {
			ratio_y = particlePtr[i].getY() - mouseY;
			dirY = -1;
		}
		double force_t = (m_gConst * mouseMass * particlePtr[i].getM()) / (pow(dist_t, 2.0));
		double force_x = force_t * ratio_x * dirX;
		double force_y = force_t * ratio_y * dirY;
		particlePtr[i].calculateTick(force_x, force_y, m_es, col);
	}
}

void GTHLD::SwarmController::manipulateParticlesThreaded(double mouseX, double mouseY, double mouseMass, Uint32 col)
{
	std::vector<std::thread> threadVecs;
	std::vector<ParticleForceCalc> calc4Threads;
	int tC = (int)m_threadCount;
	for (int i = 0; i < m_threadCount; i++) {
		ParticleForceCalc pc = ParticleForceCalc();
		pc.m_es = m_es;
		pc.m_threadCount = m_threadCount;
		pc.m_particlesPerThread = m_particlesPerThread;
		pc.m_particlesInLastThread = m_particlesInLastThread;
		calc4Threads.push_back(pc);
		threadVecs.push_back(std::thread(applyParticleForces, mouseX, mouseY, mouseMass, col, i, m_threadStarts[i], m_particlesPerThread, m_threadCount, m_particlesInLastThread, m_gConst, m_es));
	}
	for (int i = 0; i < m_threadCount; i++) {
		threadVecs[i].join();
	}
}


