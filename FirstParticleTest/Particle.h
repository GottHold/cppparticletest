#pragma once
#ifndef PARTICLE_H_
#define PARTICLE_H_
#include <EngineSystem.h>
#include <iostream>
namespace GTHLD {

	class Particle
	{
	private:
		double m_x;
		double m_y;
		double m_vel_x;
		double m_vel_y;
		double m_mass;
		const double m_x_max = (double)EngineSystem::SCREEN_WIDTH / (double)EngineSystem::SCREEN_HEIGHT;
	public:
		double getX() { return (Uint16)(m_x*(double)EngineSystem::SCREEN_HEIGHT); }
		double getY() { return (Uint16)(m_y*(double)EngineSystem::SCREEN_HEIGHT); }
		double getM() { return (Uint64)m_mass; }
		Particle();
		Particle(double x, double y, double mass, Uint32 col);
		Uint32 m_c;
		//virtual ~Particle();
		void calculateTick(double x_acceleration, double y_acceleration, EngineSystem *es, Uint32 col);
	};

}

#endif