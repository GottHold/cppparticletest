#include "EngineSystem.h"


namespace GTHLD {

	EngineSystem::EngineSystem() :
		m_buffer_1(NULL), m_buffer_2(NULL), m_renderer(NULL), m_texture(NULL), m_window(NULL) {

	}

	bool EngineSystem::init() {

		SDL_Init(SDL_INIT_VIDEO);
		m_window = SDL_CreateWindow("No U!",
			50, 50, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN);

		if (m_window == NULL) {
			std::cout << SDL_GetError() << "\r\nAbove the error." << std::endl;
			stop();
			return false;
		}

		m_renderer = SDL_CreateRenderer(m_window, -1, 1);
		m_texture = SDL_CreateTexture(m_renderer, SDL_PIXELFORMAT_RGB888, SDL_TEXTUREACCESS_STATIC, SCREEN_WIDTH, SCREEN_HEIGHT);

		if (m_renderer == NULL ||
			m_texture == NULL) {
			std::cout << "Init Error: " << SDL_GetError() << std::endl;
			SDL_DestroyTexture(m_texture);
			SDL_DestroyRenderer(m_renderer);
			SDL_DestroyWindow(m_window);
			stop();
			return false;
		}
		m_buffer_1 = new Uint32[SCREEN_HEIGHT * SCREEN_WIDTH];
		m_buffer_2 = new Uint32[SCREEN_HEIGHT * SCREEN_WIDTH];

		updateImage();

		return true;
	}

	void EngineSystem::stop() {
		delete[] m_buffer_1;
		delete[] m_buffer_2;
		SDL_DestroyTexture(m_texture);
		SDL_DestroyRenderer(m_renderer);
		SDL_DestroyWindow(m_window);
		SDL_Quit();
	}

	bool EngineSystem::processEvents() {
		SDL_Event evnt;
		while (SDL_PollEvent(&evnt)) {
			if (evnt.type == SDL_QUIT) {
				return false;
			}
		}
		return true;
	}

	void GTHLD::EngineSystem::boxBlur(Uint32 boxSize)
	{
		Uint32* buffer_temp = m_buffer_1;
		m_buffer_1 = m_buffer_2;
		m_buffer_2 = buffer_temp;

		for (int y = 0; y < SCREEN_HEIGHT; y++) {
			for (int x = 0; x < SCREEN_WIDTH; x++) {
				if ((x-boxSize) >= 0 && (y-boxSize) >= 0 && y < (SCREEN_HEIGHT - boxSize) && x < (SCREEN_WIDTH - boxSize)) {
					Uint32 r=0;
					Uint32 g=0;
					Uint32 b=0;
					Uint32 totalElements = (boxSize * 2 + 1) * (boxSize*2+1);
					for (int row = y - boxSize; row < y + boxSize + 1; row++) {
						for (int col = x - boxSize; col < x + boxSize + 1; col++) {
							Color c;
							c.color = m_buffer_2[row * SCREEN_WIDTH + col];
							r += (Uint32)c.r;
							g += (Uint32)c.g;
							b += (Uint32)c.b;
						}
					}
					Color blurred;
					r = r / totalElements;
					g = g / totalElements;
					b = b / totalElements;
					if (r > 0xFF)r = 0xff;
					if (g > 0xFF)g = 0xff;
					if(b > 0xff) b = 0xff;
					blurred.r = (Uint8)r;
					blurred.g = (Uint8)g;
					blurred.b = (Uint8)b;
					blurred.a = 0xff;
					m_buffer_1[y * SCREEN_WIDTH + x] = blurred.color;
				}
			}
		}
	}

	void EngineSystem::movementBlur(int bits) {

		for (int i = 0; i < EngineSystem::SCREEN_HEIGHT * EngineSystem::SCREEN_WIDTH; i++) {
			Color c;
			c.color = m_buffer_1[i];
			c.b >>= 1;
			c.r >>= 1;
			c.g >>= 1;
			m_buffer_1[i] = c.color;
		}
	}

	void EngineSystem::updateImage()
	{
		Uint32 nowTicks = SDL_GetTicks();
		DELTATIME = nowTicks - m_lastTicks;
		m_lastTicks = nowTicks;
		SDL_UpdateTexture(m_texture, NULL, m_buffer_1, SCREEN_WIDTH * sizeof(Uint32));
		SDL_RenderClear(m_renderer);
		SDL_RenderCopy(m_renderer, m_texture, NULL, NULL);
		SDL_RenderPresent(m_renderer);
	}

	void EngineSystem::setPixel(Uint16 x, Uint16 y, Uint8 r, Uint8 g, Uint8 b, Uint8 a, colorMode cm1)
	{
		setPixel(x, y, getColor(r, g, b, a), cm1);
	}

	void EngineSystem::setPixel(Uint16 x, Uint16 y, Uint32 col, colorMode cm)
	{
		//std::cout << x << " " << y << std::endl;
		Uint32 ogColor = m_buffer_1[y * SCREEN_WIDTH + x];
		Uint32 restAll;
		switch (cm)
		{
		case GTHLD::colorMode::normal:
			ogColor = col;
			break;
		case GTHLD::colorMode::addative:
			Color c;
			c.color = ogColor;
			Color add;
			add.color = col;
			Color final;
			final.a = 0xff;
			Uint8 restR, restG, restB, spaceR, spaceG, spaceB;
			if (add.r > (Uint8)(0xFF - c.r)) { // 230  > 255-50
				final.r = 0xff;
				restR = add.r - (0xff - c.r);
				spaceR = 0;
			}
			else {
				final.r = c.r + add.r;
				restR = 0;
				spaceR = 0xff - final.r;
			}
			if (add.g > (Uint8)(0xFF - c.g)) { // 230  > 255-50
				final.g = 0xff;
				restG = add.g - (0xff - c.g);
				spaceG = 0;
			}
			else {
				final.g = c.g + add.g;
				restG = 0;
				spaceG = 0xff - final.g;
			}
			if (add.b > (Uint8)(0xFF - c.b)) { // 230  > 255-50
				final.b = 0xff;
				restB = add.b - (0xff - c.b);
				spaceB = 0;
			}
			else {
				final.b = c.b + add.b;
				restB = 0;
				spaceB = 0xff - final.b;
			}

			restAll = restR + restG + restB;
			if (restAll > 0 && (Uint32)(spaceR + spaceG + spaceB) > 0) {
				if (spaceR > 0) {
					if (restAll >= spaceR)final.r = 0xff;
					else final.r += restAll;
				}
				if (spaceG > 0) {
					if (restAll >= spaceG)final.g = 0xff;
					else final.g += restAll;
				}
				if (spaceB > 0) {
					if (restAll >= spaceB)final.b = 0xff;
					else final.b += restAll;
				}
			}

			ogColor = final.color;


			break;
		case GTHLD::colorMode::subtractive:
			ogColor = col;
			break;
		default:
			break;
		}
		//m_changedPixel.push_back(y * SCREEN_WIDTH + x);
		m_buffer_1[y * SCREEN_WIDTH + x] = ogColor;
	}

	GTHLD::EngineSystem::~EngineSystem()
	{
		//delete m_buffer;
		//delete m_renderer;
		//delete m_texture;
		//delete m_window;
	}

	Uint32 EngineSystem::getColor(Uint8 r, Uint8 g, Uint8 b, Uint8 a)
	{
		Color col;
		col.a = a;
		col.r = r;
		col.g = g;
		col.b = b;
		return col.color;
	}

	double EngineSystem::random() {
		srand((time(NULL) * m_seedCounter * sin(m_seedCounter)) / (cos(m_seedCounter)));
		++m_seedCounter;
		auto val = (double)((double)rand() / (double)RAND_MAX);
		return val;
	}

	double EngineSystem::random(double minValue, double maxValue) {
		return random() * (maxValue - minValue) + minValue;
	}
}