// FirstParticleTest.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#define SDL_MAIN_HANDLED
#include <SDL.h>
#include <EngineSystem.h>
#include <math.h>
#include <SwarmController.h>



using namespace std;

Uint64 GTHLD::EngineSystem::DELTATIME = 0;
Uint64 GTHLD::EngineSystem::m_seedCounter = 1337;
int main()
{
	
	GTHLD::EngineSystem es;
	
	if (!es.init()) {
		cout << "Failed init" << endl;
		return -1;
	}
	GTHLD::SwarmController swarm = GTHLD::SwarmController(&es);
	Uint64 Now = SDL_GetPerformanceCounter();
	Uint64 Last;
	bool quitThisShit = false;
	int iterator = 1;
	Uint64 bucketCounter = 1;

	for (int y = 0; y < GTHLD::EngineSystem::SCREEN_HEIGHT; y++) {
		for (int x = 0; x < GTHLD::EngineSystem::SCREEN_WIDTH; x++) {
			es.setPixel(x, y, 0xff000000, GTHLD::colorMode::normal);
		}
	}
	double xI = 0.0;
	double yI = 0.0;
	double mI = 0.0;
	double rI = 0.5;
	double gI = 0.5;
	double bI = 0.5;
	static const uint32_t bhColor = GTHLD::EngineSystem::getColor(250u, 0u, 0u, 0xFFu);

	while (!quitThisShit) {
		//es.resetTexture();
		//es.movementBlur(1);
		es.boxBlur(1);
	
		
		
		if (iterator % 100 == 0) {
			
			cout << "Averaged frame time: " << bucketCounter/iterator << endl;
			cout << "Gravity: " << cos(mI)   << endl;
			iterator = 0;
			bucketCounter = 0;
			
		}
		if (!es.processEvents()) {
			break;
		}
		bucketCounter += GTHLD::EngineSystem::DELTATIME;
		
		//Game Loop here
		
		GTHLD::Color cl;
		cl.r = (Uint8)((sin(rI)+1)/2*15);
		cl.g = (Uint8)((cos(gI)+1)/2*15);
		cl.b = (Uint8)((tan(bI)+1)/2*15);
		cl.a = 0xff;

		int mouseX=(int)(((sin(xI)+1.0)/2.0*0.7+0.15)*GTHLD::EngineSystem::SCREEN_WIDTH);
		int mouseY = (int)(((cos(yI) + 1.0) / 2.0*0.7+0.15) *GTHLD::EngineSystem::SCREEN_HEIGHT);
		
		for (int i = -1; i < 2; i++) {
			for (int j = -1; j < 2; j++) {
				es.setPixel(mouseX + i, mouseY + j, bhColor, GTHLD::colorMode::normal);
			}

		}

		//SDL_GetMouseState(&mouseX, &mouseY);
		swarm.manipulateParticlesThreaded(mouseX, mouseY, ((cos(mI) + 1.0) / 2.0) * 100000000000, cl.color); //10000000000

		es.updateImage();
		++iterator;

		
		xI += 0.00002*4*(double)GTHLD::EngineSystem::DELTATIME;
		yI += 0.000003 * (double)GTHLD::EngineSystem::DELTATIME;
		mI += 0.00001 * (double)GTHLD::EngineSystem::DELTATIME;
		rI += 0.00003 * (double)GTHLD::EngineSystem::DELTATIME;
		gI += 0.00014 * (double)GTHLD::EngineSystem::DELTATIME;
		bI += 0.0001 * (double)GTHLD::EngineSystem::DELTATIME;
		xI = 0.0f;
		yI = 1.5f;
	}
	es.stop();
	return 0;
}

