#pragma once
#include <Particle.h>
#include <thread>
#include <vector>


#ifndef SWARMCONTROLLER_H_
#define SWARMCONTROLLER_H_

namespace GTHLD {

	struct ParticleForceCalc {

		EngineSystem* m_es;
		const double m_gConst = 0.000000000067;
		unsigned int m_threadCount;
		Uint32 m_particlesPerThread;
		Uint32 m_particlesInLastThread;
		// (double mouseX, double mouseY, double mouseMass, Uint32 col, int thread, Particle* particlePtr);
	};

	class SwarmController
	{
	private:
		Particle* m_pParticle;
		std::vector<Particle*> m_threadStarts;
		const int NPARTICLE = 750000;
		EngineSystem* m_es;
		const double m_gConst = 0.000000000067;
		unsigned int m_threadCount;
		Uint32 m_particlesPerThread;
		Uint32 m_particlesInLastThread;
		
	public:
		SwarmController(EngineSystem *es);
		virtual ~SwarmController();
		Particle* getParticles() { return m_pParticle; }
		void manipulateParticles(double mouseX, double mouseY, double mouseMass, Uint32 col);
		void manipulateParticlesThreaded(double mouseX, double mouseY, double mouseMass, Uint32 col);
	};

	

}


#endif

