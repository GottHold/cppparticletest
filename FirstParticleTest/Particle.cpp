#include "Particle.h"

namespace GTHLD {

	Particle::Particle()
	{
		m_x = EngineSystem::random(0,m_x_max);
		m_y = EngineSystem::random();
		m_mass = EngineSystem::random(1, 100);
		m_vel_x = 0.0;
		m_vel_y = 0.0;
		Color cr;

		cr.a = 0xff;
		cr.r = 0x00;// EngineSystem::random(0, 255);
		cr.g = EngineSystem::random(0, 255);
		cr.b = 0x00;// EngineSystem::random(0, 255);
		m_c = cr.color;
		//10000
		
		//std::cout << (Uint16)(m_x*EngineSystem::SCREEN_WIDTH) << " " << (Uint16)(m_y*EngineSystem::SCREEN_HEIGHT) << std::endl;
	}

	GTHLD::Particle::Particle(double x, double y, double mass, Uint32 col)
	{
		m_x = x;
		m_y = y;
		m_c = col;
		m_mass = mass;
	}

	void GTHLD::Particle::calculateTick(double x_force, double y_force, EngineSystem *es, Uint32 col)
	{
		double x_old = m_x;
		double y_old = m_y;
		double x_a = x_force / m_mass;
		double y_a = y_force / m_mass;
		m_x = (0.5 * x_a * pow((double)EngineSystem::DELTATIME/1000, 2.0) + m_vel_x * ((double)EngineSystem::DELTATIME/1000) + x_old);
		m_y = (0.5 * y_a * pow((double)EngineSystem::DELTATIME/1000, 2.0) + m_vel_y * ((double)EngineSystem::DELTATIME/1000) + y_old);
		m_vel_x = (m_x - x_old)/((double)EngineSystem::DELTATIME/1000);
		m_vel_y = (m_y - y_old)/((double)EngineSystem::DELTATIME/1000);
		if (m_x < 0.0)m_x = 0.0;
		if (m_x >= m_x_max)m_x =m_x_max-0.0001;
		if (m_y < 0.0)m_y = 0.0;
		if (m_y >= 1.0)m_y = 0.9999;
		//if (x_acceleration > 0)m_x = x_old + 0.001;
		//else m_x = x_old - 0.001;
		//if (y_acceleration > 0)m_y = y_old + 0.001;
		//else m_y = y_old - 0.001;

		es->setPixel((Uint16)(m_x*EngineSystem::SCREEN_HEIGHT), (Uint16)(m_y*EngineSystem::SCREEN_HEIGHT), col, GTHLD::colorMode::addative);
		//std::cout << m_x*EngineSystem::SCREEN_WIDTH << " " << m_y*EngineSystem::SCREEN_HEIGHT << " " << x_a << " " << y_acceleration << std::endl;

	}



}