#pragma once
#include <iostream>
#include <SDL.h>
#include <stdlib.h>
#include <time.h>
#include <list>

#ifndef ENGINESYSTEM_H_
#define ENGINESYSTEM_H_

namespace GTHLD
{
	enum colorMode { normal, addative, subtractive };
	typedef union Color {
		struct {
			Uint8 b;
			Uint8 g;
			Uint8 r;
			Uint8 a;
		};
		Uint32 color;
	}Color;
	class EngineSystem
	{
	private:
		SDL_Window* m_window;
		SDL_Renderer* m_renderer;
		SDL_Texture* m_texture;
		Uint32* m_buffer_1;
		Uint32* m_buffer_2;
		static Uint64 m_seedCounter;
		Uint32 m_lastTicks = 0;
		std::list<Uint32> m_changedPixel;
	public:
		const static int SCREEN_WIDTH = 2560;
		const static int SCREEN_HEIGHT = 1440;
		static Uint64 DELTATIME;
		EngineSystem();
		~EngineSystem();
		void resetTexture() { memset(m_buffer_1, 0, SCREEN_HEIGHT * SCREEN_WIDTH * sizeof(Uint32)); }
		void movementBlur(int bits);
		void boxBlur(Uint32 boxSize);
		bool init();
		void stop();
		bool processEvents();
		void updateImage();
		static double random();
		static double random(double minValue, double maxValue);
		void setPixel(Uint16 x, Uint16 y, Uint8 r, Uint8 g, Uint8 b, Uint8 a, colorMode cm);
		void setPixel(Uint16 x, Uint16 y, Uint32 col, colorMode cm);
		static Uint32 getColor(Uint8 r, Uint8 g, Uint8 b, Uint8 a);
		
	};
	
	
}

#endif